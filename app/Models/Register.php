<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Register extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    // protected $dateFormat = 'd-m-Y';
    // protected $casts = [
    //     'birth_day'  => 'date:d M Y',
    //     'created_at' => 'datetime:d-M-Y H:i:s',
    // ];
    protected $fillable = [
        'counter','name','birth_place','birth_day','gender','marriage','religion','occupation','address','type'
    ];
   
}