<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\JenisSurat;
use Response;
use DateTime;
use DB;

class JenisSuratController
{
    protected function formatTanggalToDB($value)
    {
        return ( strlen($value) > 0 ? DateTime::createFromFormat('d M Y', $value)->format('d M Y') : null );
    }

    protected function formatTimeToDB($value)
    {
        return ( strlen($value) > 0 ? DateTime::createFromFormat('d M Y H:i:s', $value)->format('Y-m-d H:i:s') : null );
    }
  

    public function getJenisSurat(Request $request){
        $data = JenisSurat::all();
        return response()->json($data);
    }

}