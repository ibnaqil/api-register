<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Register;
use Response;
use DateTime;
use DB;

class RegisterController
{
    protected function formatTanggalToDB($value)
    {
        return ( strlen($value) > 0 ? DateTime::createFromFormat('d M Y', $value)->format('d M Y') : null );
    }

    protected function formatTimeToDB($value)
    {
        return ( strlen($value) > 0 ? DateTime::createFromFormat('d M Y H:i:s', $value)->format('Y-m-d H:i:s') : null );
    }
  
    public function addRegisterRow(Request $request)
	{
        $credentials = $request->all();

        $register = new Register;

        try{
            $lastValue = Register::max('counter') ?? 0;
            // $lastValue = $this->formatTanggalToDB($request->birth_day);

            $register->counter = $lastValue + 1;
            $register->name = $request->name;
            $register->birth_place = $request->birth_place;
            $register->birth_day = $request->birth_day;
            $register->gender = $request->gender;
            $register->marriage = $request->marriage;
            $register->religion = $request->religion;
            $register->occupation = $request->occupation;
            $register->address = $request->address;
            $register->type = $request->type;
            $register->created_by = auth()->user()->id;
            $register->updated_by = auth()->user()->id;

            $register->save();
        }
        catch(Exception $e){
            return response()->json(['error' => 'network error'], 500);
        }

        return response()->json(compact('register'));
    }

    public function editRegisterRow(Request $request)
	{
        $credentials = $request->all();

        $register = Register::find($request->id);

        try{
            $register->name = $request->name;
            $register->birth_place = $request->birth_place;
            $register->birth_day = $request->birth_day;
            $register->gender = $request->gender;
            $register->marriage = $request->marriage;
            $register->religion = $request->religion;
            $register->occupation = $request->occupation;
            $register->address = $request->address;
            $register->type = $request->type;
            $register->updated_by = auth()->user()->id;

            $register->save();
        }
        catch(Exception $e){
            return response()->json(['error' => 'network error'], 500);
        }

        $counter = $register->counter;
        return response()->json(compact('register'));
    }

    public function getRegisterNumber(Request $request){
        $last = Register::OrderBy('counter','desc')
                ->select('id','counter','name', DB::raw('DATE_FORMAT(created_at, "%d-%m-%Y %H:%i") as formatted_created_at '))        
                ->first();
        return response()->json(compact('last'));
    }

    public function getRegisters(Request $request){
        $data = Register::OrderBy('created_at','desc')
                ->select('registers.*', DB::raw('DATE_FORMAT(created_at, "%d-%m-%Y %H:%i") as formatted_created_at'))
                ->get();
        return response()->json($data);
    }

}