<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\User;
use Response;

class AuthController
{


    // protected function formatTanggalToDB($value)
    // {
    //     return ( strlen($value) > 0 ? DateTime::createFromFormat('d M Y', $value)->format('Y-m-d') : null );
    // }

    // protected function formatTimeToDB($value)
    // {
    //     return ( strlen($value) > 0 ? DateTime::createFromFormat('d M Y H:i:s', $value)->format('Y-m-d H:i:s') : null );
    // }

  
    public function login(Request $request)
	{
        $credentials = $request->only('username', 'password');

            try {
                if (! $token =  auth()->attempt($credentials)) {

                    return response()->json(['error' => 'invalid_credentials'], 400);
                }
            } catch (JWTException $e) {
                return response()->json(['error' => 'could_not_create_token'], 500);
            }

            $user = auth()->user();
          
            return response()->json(compact('token','user'));
            // return $this->respondWithToken($token);
        
    }

     /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    
    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
        ]);
    }

    public function checkToken(){
        $user = auth()->user();
        $auth = auth();
        // return $this->respondWithToken($token);
        return response()->json(compact('auth'));
    }

    public function getTokenById($id){
        $token = auth()->tokenById($id);
        // return $this->respondWithToken($token);
        return response()->json(compact('token'));
    }


    public function register(Request $request)
    {
        //validate incoming request 
        // $this->validate($request, [
        //     'name' => 'required|string',
        //     'email' => 'required|email|unique:users',
        //     'password' => 'required|confirmed',
        // ]);

        try {

            $user = new User;
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $plainPassword = $request->input('password');
            $user->password = app('hash')->make($plainPassword);

            $user->save();

            //return successful response
            return response()->json(['user' => $user, 'message' => 'CREATED'], 201);

        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'User Registration Failed!'], 409);
        }

    }

}