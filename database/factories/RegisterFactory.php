<?php

namespace Database\Factories;

use App\Models\Register;
use Illuminate\Database\Eloquent\Factories\Factory;

class RegisterFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Register::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'birth_place' => $this->faker->city,
            'birth_day' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'sex' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'sex' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
            
        ];
    }

//     id
// counter
// name
// birth_place
// birth_day
// sex
// marriage
// religion
// occupation
// address
// type
// created_by
// created_at
// updated_at
}

