<?php

namespace Database\Factories;

use App\Models\JenisSurat;
use Illuminate\Database\Eloquent\Factories\Factory;

class JenisSuratFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = JenisSurat::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,            
        ];
    }

//     id
// counter
// name
// birth_place
// birth_day
// sex
// marriage
// religion
// occupation
// address
// type
// created_by
// created_at
// updated_at
}

