<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->post('login', 'AuthController@login');
$router->post('logout', 'AuthController@logout');
// $router->get('token-by-id', 'AuthController@tokenById');

// $router->get('token/{id}', 'AuthController@getTokenById');



$router->group(['middleware' => 'auth'], function () use ($router) {
    $router->post('add-register-row', 'RegisterController@addRegisterRow');
    $router->post('edit-register-row', 'RegisterController@editRegisterRow');
    $router->get('get-register-number', 'RegisterController@getRegisterNumber');
    $router->get('get-registers', 'RegisterController@getRegisters');
    $router->get('check-token', 'AuthController@checkToken');
    $router->get('get-jenis-surat', 'JenisSuratController@getJenisSurat');
});